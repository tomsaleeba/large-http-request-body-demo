const theBtn = document.getElementById('the-btn')
const theResults = document.getElementById('the-results')
const theNumInput = document.getElementById('mb-to-send')
const theTypeSelect = document.getElementById('type-of-body')

theBtn.addEventListener('click', async () => {
  clearLog()
  const mbToSend = parseInt(theNumInput.value)
  const typeOfBody = theTypeSelect.value
  const bodyGenerators = {
    binary: getBinaryRequest,
    text: getStringRequest,
    form: getFormDataRequest,
  }
  const bodyGenerator = bodyGenerators[typeOfBody]
  uiLog('Generating body')
  const {headers, body, urlSuffix} = await bodyGenerator(mbToSend * 1024 * 1024)
  const url = (() => {
    const baseUrl = '/sendymcsendface'
    if (!urlSuffix) {
      return baseUrl
    }
    return baseUrl + urlSuffix
  })()
  uiLog('Performing fetch')
  const resp = await fetch(url, {
    method: 'POST',
    headers,
    body,
  })
  uiLog('Parsing response')
  const respJson = await resp.json()
  uiLog(JSON.stringify(respJson, null, 2))
  uiLog('All done')
})

function clearLog() {
  theResults.textContent = `(cleared ${new Date().toISOString()})`
}

function uiLog(msg) {
  theResults.textContent += '\n' + msg
}

async function getStringRequest(lengthInBytes) {
  const stream = getReadableRandomAsciiStream(lengthInBytes)
  const reader = stream.getReader()
  let body = ''
  while (true) {
    const {done, value} = await reader.read()
    if (done) {
      break
    }
    body += value
  }
  console.log('Summary of string body', body.substr(0, 5), '...', body.substr(-5))
  const headers = {'Content-Type': 'text/plain'}
  return {headers, body}
}

async function getFormDataRequest(lengthInBytes) {
  const body = new FormData()
  body.set('smallTextField', 'hello, here is some text')
  body.set('numberField', 123) // will be forced to string
  const theBlob = (() => {
    const arrayJsonString = `[
      137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13,
      73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1,
      8, 6, 0, 0, 0, 31, 21, 196, 137, 0, 0, 0,
      13, 73, 68, 65, 84, 120, 218, 99, 100, 248, 255, 191,
      30, 0, 5, 132, 2, 127, 194, 91, 30, 42, 0, 0,
      0, 0, 73, 69, 78, 68, 174, 66, 96, 130
    ]`
    const dataArray = new Uint8Array(JSON.parse(arrayJsonString))
    return new Blob([dataArray], { type: 'image/png' })
  })()
  body.append('photo', theBlob, 'imageOne')
  body.append('photo', theBlob, 'imageTwo')
  body.set('someJson', new Blob([JSON.stringify({
    foo: 'bar',
    aNumber: 666,
    anArray: [11,22,33],
    getThisValue: Date.now(),
  })], { type: 'application/json' }))
  // console.log('Summary of string body', body.substr(0, 5), '...', body.substr(-5))
  const headers = {} // fetch will set content-type
  return {headers, body, urlSuffix: '/form'}
}

// thanks https://mdn.github.io/dom-examples/streams/simple-random-stream/
function randomChars(length) {
  let result = ''
  let choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()'
  for (let i = 0; i < length; i++) {
    result += choices.charAt(Math.floor(Math.random() * choices.length))
  }
  return result
}

// the spec says we can send a stream as the body, but it's not implemented yet
// https://github.com/whatwg/fetch/issues/978
function getReadableRandomAsciiStream(lengthInBytes) {
  const blockSize = 512
  let bytesSent = 0
  const stream = new ReadableStream({
    start(controller) {
      if (lengthInBytes < blockSize) {
        controller.enqueue(randomChars(lengthInBytes))
        controller.close()
        return
      }
      controller.enqueue(randomChars(blockSize))
      bytesSent += blockSize
    },
    pull(controller) {
      const remainingBytes = lengthInBytes - bytesSent
      // console.log(`Bytes remaining to enqueue: ${remainingBytes}`)
      const isNotFullBlockLeft = remainingBytes < blockSize
      if (isNotFullBlockLeft) {
        controller.enqueue(randomChars(remainingBytes))
        bytesSent += remainingBytes
        controller.close()
        console.log(`Stream had ${bytesSent} bytes enqueued`)
        return
      }
      controller.enqueue(randomChars(blockSize))
      bytesSent += blockSize
    },
  })
  return stream
}

function getBinaryRequest(lengthInBytes) {
  const body = new Uint8Array(lengthInBytes)
  for (let i = 0; i < lengthInBytes; i++) {
    body[i] = Math.floor(Math.random() * 256)
  }
  const head = body.slice(0, 5).map(e => e.toString(16)).join(',')
  const tail = body.slice(-5).map(e => e.toString(16)).join(',')
  console.log('Summary of binary body', head, '...', tail)
  const headers = {'Content-Type': 'application/octet-stream'}
  return {headers, body}
}
