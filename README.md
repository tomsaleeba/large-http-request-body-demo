This is a test to understand how various browsers handle sending a large HTTP
request. Large in this case is about 100MB but you can test different sizes to
see where the limit is (if at all).

# How to run locally

1. clone the repo
1. install deps
    ```bash
    pnpm install
    # or you can use `yarn`
    ```
1. start the server
    ```bash
    pnpm start
    # OR, to watch for code changes
    pnpm start:watch
    # you can also override the default port
    PORT=3003 pnpm start
    ```
1. open the UI using the link that is printed in the console

# How to run anywhere
This repo builds a Docker image as part of CI, so you can run this app on any
machine with Docker installed. Use this command:
```bash
docker run --rm -it -p 3000:3000 registry.gitlab.com/tomsaleeba/large-http-request-body-demo
# or, if you have this repo cloned
pnpm docker:run
```
