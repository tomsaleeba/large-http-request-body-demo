;(async () => {
  console.log('Creating stream')
  const stream = new ReadableStream({
    start(controller) {
      controller.enqueue('some data blah blah')
      controller.close()
    }
  })
  console.log('Getting reader')
  const reader = stream.getReader()
  console.log('Reading data')
  const data = await new Promise(async resolve => {
    let result = ''
    while (true) {
      console.log('Reading a chunk')
      const {done, value} = await reader.read()
      if (done) {
        console.log('Reading is done')
        return resolve(result)
      }
      result += value
    }
  })
  console.log('Data from stream:')
  console.log(data)
})()
