import fs from 'fs'
import Stream from 'stream'
import Koa from 'koa'
import router from 'koa-router'
import formidable from 'formidable'

const app = new Koa()
const routes = router()
const port = process.env.PORT || 3000

const apiHost = '127.0.0.1'

routes.get('/', ctx => {
  ctx.body = fs.readFileSync('./views/index.html').toString()
  ctx.type = 'text/html'
  ctx.length = ctx.body.length
})

routes.get('/script.js', ctx => {
  const respBody = fs.readFileSync('./views/script.js').toString()
  ctx.body = `;(function init() {\n${respBody}\n})()`
  ctx.type = 'text/javascript'
  ctx.length = ctx.body.length
})

routes.post('/sendymcsendface', async ctx => {
  const contentType = ctx.request.type
  const contentLength = ctx.req.headers['content-length']
  console.log(Date.now(), 'POST hit')
  console.log('  content-type:', contentType)
  console.log('  content-length:', contentLength)
  console.log('  user-agent:', ctx.request.headers['user-agent'])
  const totalSizeRead = await new Promise((resolve) => {
    let result = 0
    let head = null
    let tail = null
    const writableStream = new Stream.Writable()
    writableStream._write = (chunk, encoding, next) => {
      if (!head) {
        head = chunk
      }
      tail = chunk
      result += chunk.length
      return next()
    }
    ctx.req.pipe(writableStream)
    writableStream.on('close', () => {
      ;(() => {
        switch (contentType) {
          case 'text/plain':
            console.log('  Summary of ascii body',
              new String(head.slice(0, 5)).toString(),
              '...',
              new String(tail.slice(-5)).toString())
            return
          case 'application/octet-stream':
            const parsedHead = head.slice(0, 5).map(e => e.toString(16)).join(',')
            const parsedTail = tail.slice(-5).map(e => e.toString(16)).join(',')
            console.log('  Summary of binary body', parsedHead, '...', parsedTail)
            return
        }
        throw new Error(`Unhandled content-type: ${contentType}`)
      })()
      return resolve(result)
    })
  })
  ctx.body = {
    totalSizeRead,
    contentLengthFromReq: contentLength,
    timestamp: Date.now()
  }
  // FIXME look at
  // https://developer.mozilla.org/en-US/docs/Web/API/fetch#keepalive to see if
  // it can keep things going, although it probably doesn't matter with a SW
})

routes.post('/sendymcsendface/form', async ctx => {
  const form = formidable({ multiples: true })
  await new Promise((resolve, reject) => {
    form.parse(ctx.req, (err, fields, files) => {
      if (err) {
        return reject(err)
      }
      let gottenValue = null
      const pathOfJson = files['someJson'].filepath
      if (pathOfJson) {
        const rawData = fs.readFileSync(pathOfJson)
        const parsed = JSON.parse(rawData)
        gottenValue = parsed.getThisValue
      }
      // cleaning up the written files
      for (const curr of Object.values(files)) {
        if (curr.constructor === Array) {
          curr.forEach(e => fs.rmSync(e.filepath))
          continue
        }
        fs.rmSync(curr.filepath)
      }
      ctx.set('Content-Type', 'application/json')
      ctx.status = 200
      ctx.state = { fields, files, gottenValue }
      ctx.body = JSON.stringify(ctx.state, null, 2)
      return resolve()
    })
  })
})

app.use(routes.routes())
console.log(`Open the UI at http://localhost:${port}`)
app.listen(port)
