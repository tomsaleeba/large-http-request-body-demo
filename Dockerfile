FROM node:14.16-alpine
RUN npm i -g pnpm
WORKDIR /app
RUN chown -R node:node ./
USER node:node
ADD package.json pnpm-lock.yaml ./
RUN pnpm install --frozen-lockfile --prod
ADD index.mjs ./
ADD views/ ./views/

ENTRYPOINT ["pnpm", "start"]
